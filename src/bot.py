#!/usr/bin/env python

"""Runs a discord bot that checks for reposts in given channels. Currently
using imagehash libraries for early builds before writing more efficient
code. Make sure that the config.yaml is properly configured before running
the file.
"""

__version__ = "0.0"
__author__ = "John Madden"


# pylint: disable=too-many-locals
# pylint: disable=inconsistent-return-statements
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements
# pylint: disable=bad-whitespace


# system imports
import time
from argparse import ArgumentParser

# external imports
import discord
from discord.ext import commands
import numpy as np
from psycopg2.extensions import register_adapter, AsIs

# internal imports
from libhash import hash_attachment
from db import DatabaseManager
from db import Image, Channel



# reqiured for numpyint64 to work with BigInt
register_adapter(np.int64, AsIs)


# create discord bot
bot = commands.Bot(command_prefix=".")


@bot.event
async def on_ready():
    """print when ready"""

    print("logged in as {0.user}".format(bot))

#
# checks
#

async def is_indexed(ctx):
    """check if channel is already indexed

    :param ctx: discord.ext.commands.Context

    :return: bool
    :rtype: bool
    """

    channel = db.get_channel(ctx.channel)
    return channel is not None


#
# commands
#

@bot.command()
async def test(ctx):
    """test command to check if bot response is working"""

    await ctx.send("test")


@bot.command()
@commands.has_permissions(manage_messages=True)
#@commands.cooldown(1, 1*60*60*24)
async def index(ctx, limit: int=500):
    """indexes the channel where the command was invoked"""

    img_counter = 0
    repost_counter = 0
    attach_counter = 0

    time1 = time.perf_counter()

    # append to the guilds table
    db.insert_channel(ctx.message)

    # index all channels when limit is 0
    if limit == 0:
        limit = None

    # indicate that is running
    async with ctx.typing():

        # loop through all messages
        async for msg in ctx.history(limit=limit, oldest_first=True):

            # check for attachments
            if len(msg.attachments) > 0:

                # loop through attachments
                for attach in msg.attachments:

                    # check duplicate attachment ids
                    query = (
                        db.session
                        .query(Image.id)
                        .filter(Image.disc_attachment_id == attach.id)
                        .scalar()
                    )
                    if query is None:

                        # gets the hash for images
                        raw = await attach.read()
                        hash_value = hash_attachment(
                            raw,
                            attach.url,
                            attach.filename
                        )

                        # check hash_value is valid
                        if np.any(hash_value) is None:
                            attach_counter += 1
                            continue

                        # insert image
                        img = db.insert_image(msg, attach, hash_value)

                        # inc counters
                        img_counter += 1
                        if db.check_reposts(img):
                            repost_counter += 1


    time2 = time.perf_counter()

    # response
    sum_msg = discord.Embed()
    sum_msg.description = "took {:0.3f} seconds".format(time2-time1)
    sum_msg.colour = discord.Colour.green()
    if img_counter > 0:
        sum_msg.title = "Done!"
        sum_msg.add_field(name="Images", value=str(img_counter))
        sum_msg.add_field(name="Reposts", value=str(repost_counter))
        sum_msg.add_field(name="Unknown", value=str(attach_counter))
    else:
        sum_msg.title = "No valid attachments found!"

    await ctx.send(embed=sum_msg)


@bot.command()
@commands.check(is_indexed)
@commands.has_permissions(manage_messages=True)
async def stats(ctx):
    """gets stats on the channel where invoked. will not work if channel is not
    already indexed.
    """

    images = db.get_images(ctx.channel)
    # pylint: disable=singleton-comparison
    reposts = images.filter(Image.repostof_id != None)

    stats_msg = discord.Embed()
    stats_msg.title = "Stats"
    stats_msg.colour = discord.Colour.blue()

    medals = [
        ":first_place:",
        ":second_place:",
        ":third_place:",
        ]
    desc = ""
    members = db.get_members(ctx.channel).limit(len(medals))
    if members.count() > 0:
        for idx, mem in enumerate(members):
            disc_mem = bot.get_user(mem.disc_member_id)
            desc = desc \
                + medals[idx] \
                + "{0.mention} - {1.count} reposts\n".format(disc_mem, mem)
    else:
        desc = desc + "no one has reposted"

    stats_msg.add_field(name="Leaderboard", value=desc, inline=False)

    stats_msg.add_field(name="Total Images", value=str(images.count()))
    stats_msg.add_field(name="Total Reposts", value=str(reposts.count()))

    await ctx.send(embed=stats_msg)


@bot.command()
@commands.check(is_indexed)
async def remove(ctx):
    """removes a channel from being indexed. deletes all messages related to
    the channel
    """

    db.remove_channel(ctx.channel)

    await ctx.send("channel removed")


#
# errors
#

@remove.error
@stats.error
async def indexed_error(ctx, err):
    """error when channel is not indexed"""

    if isinstance(err, commands.CheckFailure):
        await ctx.send("channel not indexed")


#
# message loop
#

@bot.event
async def on_message(message):
    """event handler for all messages

    :param message: discord.Message
    """

    # skips over its own messages
    if message.author == bot.user:
        return

    # check for reposts
    if len(message.attachments) > 0:

        # check where message came from
        channel = (
            db.session
            .query(Channel.id)
            .filter(Channel.disc_channel_id == message.channel.id)
            .scalar()
        )

        if channel is not None:
            # loop through attachments
            for attach in message.attachments:
                raw = await attach.read()
                hash_value = hash_attachment(raw, attach.url, attach.filename)

                # file unable to be hashed
                if np.any(hash_value) is None:
                    continue

                # insert image
                img = db.insert_image(message, attach, hash_value)

                if db.check_reposts(img):
                    orig = img
                    orig_channel = bot.get_channel(orig.channel.disc_channel_id)
                    orig_msg = await orig_channel.fetch_message(orig.disc_message_id)
                    orig_author = bot.get_user(orig.disc_member_id)

                    # reponse message
                    repost_msg = discord.Embed()
                    repost_msg.title = "Found Repost"
                    repost_msg.url = orig_msg.jump_url
                    repost_msg.colour = discord.Colour.red()
                    repost_msg.set_thumbnail(url=attach.url)
                    repost_msg.add_field(name="Author", value=orig_author.mention)

                    await message.channel.send(embed=repost_msg)

    # after check if command was issued
    await bot.process_commands(message)


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Run a discord bot to index images in channels and check for duplicates",
        )

    # arguments
    parser.add_argument(
        "-v", "--verbose",
        action="count", default=0,
        help="increase output verbosity"
        )
    parser.add_argument(
        "token",
        help="discord bot token"
        )
    parser.add_argument(
        "--database",
        default="sqlite3:///repostbot.db",
        help="sqlalchemy url for database to store images"
        )

    args = parser.parse_args()

    # check versions
    if args.verbose > 0:
        print(discord.__version__)
        print(args)

    # create db connection
    db = DatabaseManager(args.database)

    # run bot
    bot.run(args.token)
