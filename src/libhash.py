"""library to hash images and videos"""

# pylint: disable=inconsistent-return-statements

import os
import io

import PIL.Image
import cv2
import imagehash
import numpy as np


# supported image formats
IMG_EXTS = np.array([
    "jpeg",
    "jpg",
    "tiff",
    "tif",
    "png",
])

# supported video formats
VID_EXTS = np.array([
    "mkv",
    "flv",
    "avi",
    "mp4",
    "webm",
    "mov",
])


def _hash_video(url):
    '''hashes a video by taking the first frame of the video and treats it as
    a singular image. Discord does not allow uploading of files past 100 MB
    for non nitro users so ram should be sufficiant for the video

    also i needed to convert from the bgr to the rgb colorspace because that is
    what opencv and PIL use repectively

    :param url: a complete url to the video file
    '''

    vid = cv2.VideoCapture(url)
    if vid.isOpened():
        _, bgr_img = vid.read()

        rgb_img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2RGB)
        img = PIL.Image.fromarray(rgb_img)
        hash_value = imagehash.phash(img).hash

        return hash_value



def _hash_image(raw):
    '''hashes an image given in a BytesIO object.

    :param raw: BytesIO of image
    '''

    raw = io.BytesIO(raw)
    img = PIL.Image.open(raw)
    hash_value = imagehash.phash(img).hash
    return hash_value




def hash_attachment(raw, url, filename):
    '''hashes raw bytes of an image into a 8 byte array based on the phash
    algorithm from imagehash.

    :param raw: python bytes object
    :param url: a complete url to the video file
    :param filename: the filename to be hashed. used to determine how to
    handle the raw input
    '''

    ext = os.path.splitext(filename)[-1].lower()[1:]

    if ext in IMG_EXTS:
        hash_value = _hash_image(raw)
    elif ext in VID_EXTS:
        hash_value = _hash_video(url)
    else:
        hash_value = None

    return hash_value
