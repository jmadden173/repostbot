"""handles database functions with database manager acting as a
wrapper for discord-python objects to objects in the database"""


# pylint: disable=R0903

from sqlalchemy import Column, BigInteger, String, DateTime, LargeBinary, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy import func, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

import numpy as np


Base = declarative_base()
Session = sessionmaker()



class DatabaseManager:
    """handles the connection to the database in sqlalchemy"""

    def __init__(self, db_addr):
        """initialize the DatabaseManager

        :param db_addr: sqlalchemy database address
        """


        # create engine
        engine = create_engine(db_addr)
        # init tables
        Base.metadata.create_all(engine)

        # connect
        Session.configure(bind=engine)
        self.session = Session()


    def get_members(self, channel):
        """gets a query that contains how many reposts each user has made

        """

        # pylint: disable=singleton-comparison
        query = (
            self.session
            .query(
                Image.disc_member_id,
                func.count(Image.disc_member_id).label("count")
                )
            .join(Channel)
            .filter(Image.repostof_id != None)
            .filter(Channel.disc_channel_id == channel.id)
            .group_by(Image.disc_member_id)
            .order_by(text("count"))
            )

        return query



    def get_channel(self, channel):
        """gets the channel orm object

        :param channel: discord.Channel object

        :return chan: sqlalchemy object for channel
        """

        chan = (
            self.session
            .query(Channel)
            .filter(Channel.disc_channel_id == channel.id)
            .scalar()
            )

        return chan


    def insert_channel(self, msg):
        """inserts a channel from a discord.Message(), returns
        the channel object corresponding to the msg. checks if channel
        is already in the db

        :param msg: discord.Message from the channel to be added

        :return channel: a Channel() object
        """

        channel = (
            self.session
            .query(Channel.id)
            .filter(Channel.disc_channel_id == msg.channel.id)
            .scalar()
            )

        if channel is None:
            channel = Channel(
                disc_guild_id=msg.guild.id,
                disc_channel_id=msg.channel.id,
                disc_user_id=msg.author.id,
                )

            self.session.add(channel)
            self.session.commit()

        return channel


    def remove_channel(self, channel):
        """removes a indexed channel from the database

        :param channel: discord.TextChannel to be removed
        """

        (
            self.session
            .query(Channel)
            .filter(Channel.disc_channel_id == channel.id)
            .delete()
        )
        self.session.commit()


    def get_images(self, channel):
        """gets all images corresponding to a given channel

        :param: discord.Channel

        :return images: sqlalchemy query
        """

        images = (
            self.session
            .query(Image)
            .join(Channel)
            .filter(Channel.disc_channel_id == channel.id)
            )

        return images



    def insert_image(self, msg, attach, hash_value):
        """creates an Image object with infered values and inserts
        into db with commit()

        :param msg: discord.Message object
        :param attach: discord.Attachment object
        :param hash_value: hash_value from imagehash

        :returns: a Image object
        """

        # get channel id
        channel = (
            self.session
            .query(Channel.id)
            .filter(Channel.disc_channel_id == msg.channel.id)
            .scalar()
        )

        # create image
        hashed_image = Image(
            datetime_posted=msg.created_at,
            channel_id=channel,
            disc_member_id=msg.author.id,
            disc_message_id=msg.id,
            disc_attachment_id=attach.id,
            filename=attach.filename,
            url=attach.url,
            hash_value=hash_value,
        )

        # add to db
        self.session.add(hashed_image)
        self.session.commit()

        return hashed_image


    def check_reposts(self, input_img):
        """checks the reposts given a sqlalchemy orm object of Image

        Some weird stuff was happening where if the object was not committed
        then it would still contain a byte array for the hash_value to use
        in comparison but once it was commited it was converted to a bytes
        object that you have to convert back

        :param img: Image object already commited to db

        :return query: returns the query containing all reposts
        """

        # pylint: disable=singleton-comparison
        query = (
            self.session.query(Image)
            .join(Channel)
            .filter(Channel.disc_guild_id == input_img.channel.disc_guild_id)
            .filter(Image.id != input_img.id)
            .filter(Image.repostof_id == None)
        )

        input_barray = np.frombuffer(input_img.hash_value, dtype=np.bool)
        for img in query:
            img_barray = np.frombuffer(img.hash_value, dtype=np.bool)
            diff = np.count_nonzero(input_barray != img_barray)
            if diff < 10:
                input_img.repostof_id = img.id
                break

        self.session.commit()

        return input_img.repostof_id is not None


class Image(Base):
    """reprents a database table that stores images"""

    __tablename__ = "images"
    id = Column(BigInteger, primary_key=True)
    repostof_id = Column(BigInteger, ForeignKey("images.id"), nullable=True)
    channel_id = Column(
        BigInteger,
        ForeignKey("channels.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False
    )
    datetime_posted = Column(DateTime, nullable=False)
    datetime_added = Column(DateTime(timezone=True), server_default=func.now())
    disc_member_id = Column(BigInteger, nullable=False)
    disc_message_id = Column(BigInteger, nullable=False)
    disc_attachment_id = Column(BigInteger, nullable=False, unique=True)
    filename = Column(String, nullable=False)
    url = Column(String, nullable=False)
    hash_value = Column(LargeBinary(8), nullable=False)

    repostof = relationship("Image")
    channel = relationship("Channel", back_populates="images")


class Channel(Base):
    """represents a database table that stores indexed guilds"""

    __tablename__ = "channels"
    id = Column(BigInteger, primary_key=True)
    datetime_added = Column(DateTime(timezone=True), server_default=func.now())
    disc_guild_id = Column(BigInteger, nullable=False)
    disc_channel_id = Column(BigInteger, nullable=False, unique=True)
    disc_user_id = Column(BigInteger, nullable=False)

    images = relationship("Image", back_populates="channel")
