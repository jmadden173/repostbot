FROM python:latest

WORKDIR /app
ENV PYTHONPATH=/app

COPY src/ .
COPY requirements.txt .

RUN pip install -r requirements.txt

CMD python bot.py --database $DB_URL $CLIENT_TOKEN
